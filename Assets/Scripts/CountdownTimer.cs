﻿using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {

    const int DEFAULT_TIME_LEFT = 120;
    const string TIME_TEXT_FORMAT = "Time left: {0}:{1:00}";

    private int timeLeft = DEFAULT_TIME_LEFT;
    private bool timeUp = false;
    private AudioSource source;

    public Text timeLeftText;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void StartCountDown()
    {
        SetDefaultValues();
        UpdateText();

        InvokeRepeating("CountDown", 0, 1);
    }

    public void SetTimer(int timeLeft)
    {
        this.timeLeft = timeLeft;
    }

    public bool IsTimeUp()
    {
        return timeUp;
    }

    private void CountDown()
    {
        if (timeLeft > 0)
        {
            timeLeft--;
        }
        else
        {
            timeUp = true;
        }

        UpdateText();
    }

    private void SetDefaultValues()
    {
        timeLeftText.color = Color.white;
        timeUp = false;
    }

    private void UpdateText()
    {
        int minutes = timeLeft / 60;
        int seconds = timeLeft % 60;

        // Change the text color to red if there are only 10 seconds left
        if (timeLeft <= 10)
        {
            timeLeftText.color = Color.red;
            source.Play();
        }

        timeLeftText.text = string.Format(TIME_TEXT_FORMAT, minutes, seconds);
    }

}

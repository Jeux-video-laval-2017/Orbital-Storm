﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    const int START_LEVEL = 1;

    public void StartGame()
    {
        LevelManager.currentLevel = START_LEVEL;
        SceneManager.LoadScene("OrbitalStorm");
    }

    public void LoadSceneByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NextLevelButton : MonoBehaviour {

    const string LEVEL_TEXT_FORMAT = "Save and go to level {0}";
    const int MAX_LEVEL = 5;

    private int nextLevel;
    private Button nextLevelButton;

    public PlayerDataEditor playerDataEditor;
    public Text nextLevelText;

    void Start()
    {
        nextLevelButton = gameObject.GetComponent<Button>();

        nextLevel = (LevelManager.currentLevel + 1);
        nextLevelText.text = string.Format(LEVEL_TEXT_FORMAT, nextLevel);

        // Make button inactive if the player has reached MAX_LEVEL
        if (nextLevel > MAX_LEVEL || Planet.currentHealth <= 0)
        {
            nextLevelButton.gameObject.SetActive(false);
        }
    }

    public void GoToNextLevel ()
    {
        Save();
        SceneManager.LoadScene("OrbitalStorm");
    }

    private void Save()
    {
        int currentScore = OrbitalStation.playerScore;
        int highestScore = OrbitalStation.playerHighestScore;

        PlayerData playerData = new PlayerData();
        playerData.level = (LevelManager.currentLevel + 1);
        playerData.highestScore = (currentScore > highestScore) ? currentScore : highestScore;
 
        playerDataEditor.SavePlayerData(playerData);
    }
}

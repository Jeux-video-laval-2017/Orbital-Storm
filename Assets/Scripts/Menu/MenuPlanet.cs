﻿using UnityEngine;

public class MenuPlanet : MonoBehaviour {
    const int SPEED = 2;

    void Update()
    {
        transform.Rotate(-Vector3.forward, SPEED * Time.deltaTime);
    }
}

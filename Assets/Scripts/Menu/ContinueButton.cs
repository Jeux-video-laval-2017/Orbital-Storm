﻿using UnityEngine;
using UnityEngine.UI;

public class ContinueButton : MonoBehaviour {

    private Button continueButton;

    public PlayerDataEditor playerDataEditor;

    void Awake()
    {
        continueButton = gameObject.GetComponent<Button>();

        // If a game does not exist, the player cannot click on the button
        if (!playerDataEditor.hasSavedGame())
        {
            continueButton.interactable = false;
        }
    }

}

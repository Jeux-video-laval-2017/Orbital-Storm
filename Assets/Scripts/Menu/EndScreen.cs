﻿using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    const string LOSE_MESSAGE = "You lose!";
    const string WIN_MESSAGE = "You win!";
    const string END_MESSAGE = "You have completed the game!";
    const int MAX_LEVEL = 5;

    public Button retryButton;
    public Text scoreText;
    public Text message;

    void Start () {
        setMessage();

        // Make 'Retry button' inactive if the player didn't lose
        if (!string.Equals(message.text, LOSE_MESSAGE))
        {
            retryButton.gameObject.SetActive(false);
        }

        scoreText.text = "Score: " + OrbitalStation.playerScore.ToString();
    }

    private void setMessage()
    {
        string messageToShow = WIN_MESSAGE;
        if (LevelManager.currentLevel == MAX_LEVEL) 
        {
            messageToShow = END_MESSAGE;
        }
        else if (Planet.currentHealth <= 0)
        {
            messageToShow = LOSE_MESSAGE;
        }

        message.text = messageToShow;
    }

}

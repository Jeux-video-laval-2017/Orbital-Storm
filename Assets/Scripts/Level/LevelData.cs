﻿using System;

[Serializable]
public class LevelData 
{
    public int level;
    public int time;
    public int numberOfElementAtStart;
    public int spawningInterval;
}

﻿using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    const string LEVEL_TEXT_FORMAT = "Level {0}";

    public static int currentLevel;

    public CountdownTimer timer;
    public ElementSpawner elementSpawner;
    public LevelLoader levelLoader;
    public Text levelText;

    public void StartLevel(int level)
    {
        SetLevel(level);

        timer.StartCountDown();
        elementSpawner.StartSpawningElements();
    }

    public bool IsTimeUp()
    {
        return timer.IsTimeUp();
    }

    public int GetElementCount()
    {
        return elementSpawner.GetElementCount();
    }

    private void SetLevel(int level)
    {
        LevelData levelData = GetLevelData(level);

        timer.SetTimer(levelData.time);
        elementSpawner.SetInitialNumberOfElements(levelData.numberOfElementAtStart);
        elementSpawner.SetInterval(levelData.spawningInterval);

        currentLevel = level;
        levelText.text = string.Format(LEVEL_TEXT_FORMAT, currentLevel);
    }

    private LevelData GetLevelData(int level)
    {
        return levelLoader.GetLevelData(level);
    }

}

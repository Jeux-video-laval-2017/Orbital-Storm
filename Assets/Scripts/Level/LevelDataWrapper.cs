﻿using System;
using System.Collections.Generic;

[Serializable]
public class LevelDataWrapper
{
    public List<LevelData> levels;
}

﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class LevelLoader : MonoBehaviour {

    const string LEVEL_DATA_FILE_NAME = "levels";

    private List<LevelData> levels;

    void Awake () {
        levels = new List<LevelData>();

        LoadLevelData();
    }

    public LevelData GetLevelData(int level)
    {
        LevelData levelData = levels.Find(data => data.level == level);
        if (levelData == null)
        {
            throw new Exception("Could not load level");
        }

        return levels.Find(data => data.level == level);
    }

    private void LoadLevelData()
    {
        LevelDataWrapper wrapper = new LevelDataWrapper();

        // Load level infos from json file in 'Resources' folder
        try
        {
            TextAsset file = Resources.Load<TextAsset>(LEVEL_DATA_FILE_NAME);
            JsonUtility.FromJsonOverwrite(file.text, wrapper);
            levels = wrapper.levels;
        }
        catch (Exception exception)
        {
            throw new Exception("Could not load level file: " + exception);
        }
    }

}

﻿using UnityEngine;

public class ElementSpawner : MonoBehaviour {

    const int DELAY_TIME = 5;

    private int elementCount;
    private int interval;
    private int initialNumberOfElements;

    public Planet planet;
    public GameObject elementPrefab1;
    public GameObject elementPrefab2;
    public GameObject elementPrefab3;

    void FixedUpdate()
    {
        // Update the number of elements
        int element1Count = GameObject.FindGameObjectsWithTag("Enemy1").Length;
        int element2Count = GameObject.FindGameObjectsWithTag("Enemy2").Length;
        int element3Count = GameObject.FindGameObjectsWithTag("Enemy3").Length;

        elementCount = (element1Count + element2Count + element3Count);
    }

    public void StartSpawningElements()
    {
        SpawnElements();
        SpawnElementAtInterval();
    }

    public int GetElementCount()
    {
        return elementCount;
    }

    public void SetInterval(int interval)
    {
        this.interval = interval;
    }

    public void SetInitialNumberOfElements(int initialNumberOfElements)
    {
        this.initialNumberOfElements = initialNumberOfElements;
    }

    private void SpawnElementAtInterval()
    {
        // Wait DELAY_TIME sec. before creating an element every SPAWN_TIME sec.
        InvokeRepeating("CreateElement", DELAY_TIME, interval);
    }

    private void CreateElement()
    {
        GameObject newElement;
        int nextElement = Random.Range(1, 4);

        // Get a random position on the planet
        Vector3 spawnPosition = Random.onUnitSphere * planet.GetRadius() + transform.position;

        // Generate a pseudo-random element
        switch (nextElement)
        {
            case 1:
                newElement = Instantiate(elementPrefab1, spawnPosition, Quaternion.identity);
                break;
            case 2:
                newElement = Instantiate(elementPrefab2, spawnPosition, Quaternion.identity);
                break;
            default:
                newElement = Instantiate(elementPrefab3, spawnPosition, Quaternion.identity);
                break;
        }

        newElement.transform.parent = planet.transform;

        // Adjust orientation
        newElement.transform.LookAt(transform.position);
        newElement.transform.Rotate(-90, 0, 0);
    }

    private void SpawnElements()
    {
        for (int nbElements = 0; nbElements < initialNumberOfElements; nbElements++)
        {
           CreateElement();
        }
    }

}

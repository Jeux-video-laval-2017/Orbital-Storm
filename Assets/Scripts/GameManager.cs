﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    const int DELAY_TIME = 2;
    const float DECREASE_HEALTH_INTERVAL = 0.4f;

    public OrbitalStation orbitalStation;
    public LevelManager levelManager;
    public PlayerDataEditor playerDataEditor;
    public Planet planet;

    // init the scene
    void Start () {
        PlayerData playerData = playerDataEditor.LoadPlayerData();

        OrbitalStation.playerHighestScore = playerData.highestScore;
        levelManager.StartLevel(playerData.level);

        StartReducingHealth();
    }
	
	void Update () {
        if (levelManager.IsTimeUp() || planet.IsOver())
        {
            SceneManager.LoadScene("MenuFin");
        }
    }

    private void StartReducingHealth()
    {
        // Wait DELAY_TIME sec. before decreasing the planet's health every DECREASE_HEALTH_INTERVAL sec.
        InvokeRepeating("ReduceHealth", DELAY_TIME, DECREASE_HEALTH_INTERVAL);
    }

    private void ReduceHealth()
    {
        int elementCount = levelManager.GetElementCount();
        planet.ReduceHealth(elementCount);
    }

}

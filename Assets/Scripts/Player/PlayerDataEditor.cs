﻿using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using UnityEngine;

public class PlayerDataEditor : MonoBehaviour {

    const string PLAYER_FILE_NAME = "/player.binary";
    const int START_LEVEL = 1;
    const int NO_SCORE = 0;

    private string path;

    void Awake () {
        path = (Application.persistentDataPath + PLAYER_FILE_NAME);
    }

    public void SavePlayerData()
    {
        PlayerData playerData = new PlayerData();
        playerData.level = START_LEVEL;
        playerData.highestScore = NO_SCORE;

        SavePlayerData(playerData);
    }

    public void SavePlayerData(PlayerData playerData)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Create(path);
        formatter.Serialize(file, playerData);
        file.Close();
    }

    public PlayerData LoadPlayerData()
    {
        PlayerData playerData = new PlayerData();

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream savedFile = File.Open(path, FileMode.Open);
            playerData = (PlayerData)formatter.Deserialize(savedFile);

            savedFile.Close();
        } else
        {
            throw new Exception("Could not open file");
        }

        return playerData;
    }

    public bool hasSavedGame()
    {
        return File.Exists(path);
    }

}

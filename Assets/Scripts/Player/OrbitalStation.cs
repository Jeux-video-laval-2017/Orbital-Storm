﻿using UnityEngine;
using UnityEngine.UI;

public class OrbitalStation : MonoBehaviour
{
    const int SPEED = 10;

    private AudioSource source;
    private Rigidbody orbitalStation;
    private int weaponType;

    private enum BulletType
    {
        Ice = 0,
        Water = 1,
        Fire = 2,
    }

    public static int playerScore;
    public static int playerHighestScore;

    public PlanetGravity planetGravity;
    public Transform bulletSpawn;
    public Text scoreText;
    public Text highestScoreText;
    public Text weaponTypeText;
    public int bulletSpeed;

    public GameObject IceBullet;
    public GameObject WaterBullet;
    public GameObject FireBullet;

    void Start()
    {
        source = GetComponent<AudioSource>();
        orbitalStation = GetComponent<Rigidbody>();
        orbitalStation.useGravity = false;

        playerScore = 0;
        weaponType = 0;
        scoreText.text = "Score: " + playerScore.ToString();
        highestScoreText.text = " Highest score: " + playerHighestScore.ToString();
        weaponTypeText.text = "Weapon type: Bullet1";
    }

    void Update()
    {
        //User shot
        if (Input.GetButtonDown("Fire1"))
        {
            source.Play();
            throwBullet();
        }

        // Change weapon type
        if (Input.GetButtonDown("Fire2"))
        {
            changeWeaponTye();
        }

        weaponTypeText.text = "Bullet type: " + ((BulletType)weaponType).ToString();
        scoreText.text = "Score: " + playerScore.ToString();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(-moveVertical, 0.0f, moveHorizontal);
        Vector3 movementDirection = movement.normalized;

        orbitalStation.MovePosition(orbitalStation.transform.position + orbitalStation.transform.TransformDirection(movementDirection) * Time.deltaTime * SPEED);

        planetGravity.ApplyGravity(orbitalStation);
    }

    private void changeWeaponTye()
    {
        weaponType += 1;
        weaponType = (weaponType % 3);
    }

    private void throwBullet()
    {
        GameObject bullet;

        // Create the bullet accroding to type
        switch (weaponType)
        {
            case 0:
                bullet = Instantiate(IceBullet, bulletSpawn.position, bulletSpawn.rotation);
                break;
            case 1:
                bullet = Instantiate(WaterBullet, bulletSpawn.position, bulletSpawn.rotation);
                break;
            default:
                bullet = Instantiate(FireBullet, bulletSpawn.position, bulletSpawn.rotation);
                break;
        }

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bulletSpeed;
    }
}

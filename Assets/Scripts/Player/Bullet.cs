﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public AudioClip explosion;
    public string enemyTag;

    // Called when bullet hits something
    void OnTriggerEnter(Collider collider)
    {
        // All projectile colliding game objects should be tagged
        if (collider.gameObject.tag == enemyTag)
        {
            OrbitalStation.playerScore += 1;
            AudioSource.PlayClipAtPoint(explosion, gameObject.transform.position);
            Destroy(collider.gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }
}


﻿using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour {

    const int INITIAL_HEALTH = 100;
    const float COEFFICIENT = 0.2f;
    const int SPEED = 3;

    public static float currentHealth;

    public Slider healthSlider;

    void Start()
    {
        currentHealth = INITIAL_HEALTH;
        healthSlider.value = INITIAL_HEALTH;
    }

    void Update()
    {
        // Rotate the planet
        this.transform.Rotate(-Vector3.forward, SPEED * Time.deltaTime);
    }

    public float GetRadius()
    {
        float radius = this.transform.GetComponent<SphereCollider>().radius;
        float scale = this.transform.localScale.x;

        // Return the planet's radius
        return (scale * radius);
    }

    public void ReduceHealth(int elementCount)
    {
        float damageAmount = (elementCount * COEFFICIENT);

        currentHealth -= damageAmount;
        healthSlider.value = currentHealth;
    }

    public bool IsOver()
    {
        return (currentHealth <= 0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGravity : MonoBehaviour
{
    const float GRAVITATIONAL_CONSTANT = -6.68f;
    const int distance = 25;

    private Rigidbody planet;

    void Start()
    {
        planet = GetComponent<Rigidbody>();
        planet.useGravity = false;
    }

    public void ApplyGravity(Rigidbody body)
    {
        Vector3 distanceDifference = body.transform.position - planet.transform.position;
        float sqrDistance = distanceDifference.sqrMagnitude;

        // Find the force's direction
        Vector3 gravityDirection = distanceDifference.normalized;

        // Use Newton's law of universal gravitation to find the force to apply
        float gravitationalForce = (planet.mass * body.mass * GRAVITATIONAL_CONSTANT) / sqrDistance;

        Vector3 localUp = body.transform.TransformDirection(Vector3.up);

        if (distanceDifference.magnitude <= distance)
        {
            // Stop applying force in order to keep a distance between the planet and the body
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
        }
        else
        {
            body.AddForce(gravityDirection * gravitationalForce);
        }

        Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityDirection) * body.transform.rotation;
        body.transform.rotation = Quaternion.Slerp(body.transform.rotation, targetRotation, 50f * Time.deltaTime);
    }
}
